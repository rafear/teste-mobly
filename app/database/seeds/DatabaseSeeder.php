<?php

class DatabaseSeeder extends Seeder {

	/**
	 * Run the database seeds.
	 *
	 * @return void
	 */
	public function run()
	{
		Eloquent::unguard();

		$this->call('UsersSeeder');
		$this->command->info('Users table has been seeded!');
	}

}

class UsersSeeder extends Seeder {

	    public function run()
	    {
	        DB::table('users')->delete();

	        User::create(array('user_name' => 'admin', 'password' => Hash::make('teste123')));
	        User::create(array('user_name' => 'usuario', 'password' => Hash::make('teste123')));
	    }

	}
