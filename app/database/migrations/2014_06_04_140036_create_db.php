<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateDb extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		// Criação da tabela purchase_order
		Schema::create('purchase_order', function($table)
		{
    		$table->increments('id');
            $table->integer('order_number')->unique();
            $table->decimal('total_cost', 10, 2);
            $table->decimal('total_discount', 10, 2);
            $table->timestamps();
		});


		// Criação da tabela item
		Schema::create('item', function($table)
		{
    		$table->increments('id');
            $table->integer('item_number');
            $table->decimal('cost', 10, 2);
            $table->decimal('discount', 10, 2);
            $table->integer('purchase_order_id')->unsigned()->index();
			$table->foreign('purchase_order_id')->references('id')->on('purchase_order')->onDelete('cascade');
            $table->timestamps();
		});

		// Criação da tabela Usuario
		Schema::create('users', function($table)
		{
    		$table->increments('id');
            $table->string('user_name', 50)->unique();
            $table->string('password');
            $table->string('remember_token');            
            $table->timestamps();
		});
		
	}

	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('item');
		Schema::drop('purchase_order');
		Schema::drop('users');
	}

}
