<?php

class ListacompraController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth', array());
    }

    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function getIndex()
    {
        return View::make('listacompra.index', array('listas' => Purchase_order::get()));
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getCreate()
    {
        return View::make('listacompra.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postStore()    
    {

        $validator = Purchase_order::validate(Input::all());
        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $compra                 = new Purchase_order();
        $compra->order_number   = Input::get('order_number');
        $compra->total_cost     = 0;
        $compra->total_discount = 0;
        $compra->save();
        return Redirect::to('listacompra');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getEdit($id)
    {
        return View::make('listacompra.update', array('listaCompra' => Purchase_order::find($id)));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postUpdate($id)
    {

        $compra = Purchase_order::find($id);
        if($compra->order_number == Input::get('order_number')) {
            return Redirect::to('listacompra');
        }

        $validator = Purchase_order::validate(Input::all());
        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $compra               = Purchase_order::find($id);
        $compra->order_number = Input::get('order_number');
        $compra->save();
        return Redirect::to('listacompra');
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function getDestroy($id)
    {
        
        $compra = Purchase_order::find($id);
        $compra->delete();
        return Redirect::to('listacompra');
    }
}