<?php

class ItemController extends BaseController {

    public function __construct() {
        $this->beforeFilter('auth', []);
    }

    /**
     * Display a listing of the resource.
     * 
     * @param $compraId integer
     * @return Response
     */
    public function getIndex($compraId = null)
    {
        $parametros = ['itens' => [], 'purchase_order_id' => '' ];
        if($compraId) {
            $parametros['itens'] = Item::where('purchase_order_id', '=', $compraId)
                ->orderBy('purchase_order_id')
                ->orderBy('item_number')
                ->get();
            $parametros['purchase_order_id'] = $compraId;
        } else {
            $parametros['itens'] = Item::orderBy('purchase_order_id')->orderBy('item_number')->get();
        }
        return View::make('item.index', $parametros);        
    }   

    /**
     * Show the form for creating a new resource.
     *
     * @param $compraId integer
     * @return Response
     */
    public function getCreate($compraId = null)
    {
        $parametros = ['disable_list' => false, 'compra_id' => null];
        if($compraId) {
            $parametros['compra_id'] = $compraId;
            $parametros['disable_list'] = true;
        }
        $parametros['lista_compra'] = Purchase_order::lists('order_number', 'id');
        return View::make('item.create', $parametros);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function postStore()    
    {
        $validator = Item::validate(Input::all(), Input::get('purchase_order_id'));
        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }

        $item                    = new Item();
        $item->item_number       = Input::get('item_number');
        $item->cost              = Input::get('cost');
        $item->discount          = Input::get('discount');
        $item->purchase_order_id = Input::get('purchase_order_id');
        $item->save();
        Purchase_order::atualiza_valores(Input::get('purchase_order_id'));
        return Redirect::to('item/index/'.Input::get('compra_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function getEdit($id, $compra_id = null)
    {
        return View::make('item.update', array('item' => Item::find($id), 'compra_id' => $compra_id));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function postUpdate($id)
    {
        Validator::extend('custom_unique', function($attribute, $value, $parameters)
        {
            $total = Item::where('item_number', '=', $value)
                ->where('purchase_order_id', '=', $parameters[0])
                ->where('id', '!=', $parameters[1])
                ->count();
            if($total > 0) {
                return false;
            }  else {
                return true;
            }
        });


        $item = Item::find($id);
        $validator = Validator::make(
            Input::all(),
            array(
                'item_number' => 'required|integer|max:999|custom_unique:'.$item->purchase_order_id.",$id",
                'cost' => 'required|numeric',
                'discount' => 'required|numeric'
            )
        );

        if ($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);

        }

        $item->item_number       = Input::get('item_number');
        $item->cost              = Input::get('cost');
        $item->discount          = Input::get('discount');
        $item->save();
        Purchase_order::atualiza_valores($item->purchase_order_id);
        return Redirect::to('item/index/'.Input::get('compra_id'));
    
    }


    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function getDestroy($id, $compra_id = null)
    {
        $item = Item::find($id);
        $item->delete();
        Purchase_order::atualiza_valores($item->purchase_order_id);
        return Redirect::to('item/index/'.$compra_id);
    }
}