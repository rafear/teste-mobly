<?php

class UploadController extends BaseController {

    public function __construct() {
    }

    /**
     * Upload de xml selecionado.
     * O arquivo deve estar no diretorio app/storage/xml
     * 
     * @param $compraId integer
     * @return Response
     */
    public function getIndex()
    {

        $file = file_get_contents(app_path().'/storage/xml/test.xml');
        $xml = simplexml_load_string($file);
        $erros = [];


        foreach ($xml->order as $order) {
            
            $arrOrder = (array) $order;
            $arrOrder['order_number'] = $arrOrder['@attributes']['order_number'];
            $validator = Purchase_order::validate($arrOrder);

            if ($validator->fails()) {                
                $erros[] = [
                    'texto' => "Na Compra ordem {$arrOrder['order_number']}",
                    'classe' => $validator->messages()
                ];
                continue;
            }

            $compra = new Purchase_order();
            $compra->order_number = $arrOrder['@attributes']['order_number'];
            $compra->created_at = $order->created_at;
            $compra->updated_at = $order->updated_at;
            $compra->total_cost = $arrOrder['cost'];
            $compra->total_discount = $arrOrder['discount'];
            $compra->save();

            //importando itens
            $arrItens = (array) $order->items;
            $arrItens = $arrItens['item'] == is_array($arrItens['item']) ? $arrItens['item'] : array($arrItens['item']);
            foreach ($arrItens as $item) {

                $arrItem = (array) $item;
                $arrItem['item_number'] = (integer)$arrItem['@attributes']['item_number'];
                $arrItem['purchase_order_id'] = $compra->id;
                $validator = Item::validate($arrItem, $compra->id);
                
                if ($validator->fails()) {
                    $erros[] = [
                        'texto' => "No item ordem {$arrItem['item_number']} da Compra ordem {$arrOrder['order_number']}",
                        'classe' => $validator->messages()
                    ];
                    continue;
                }
                
                $itemEnt = new Item();
                $itemEnt->item_number       = $arrItem['item_number'];
                $itemEnt->cost              = $item->cost;
                $itemEnt->discount          = $item->discount;
                $itemEnt->created_at        = $item->created_at;
                $itemEnt->updated_at         = $item->updated_at;
                $itemEnt->purchase_order_id = $compra->id;
                $itemEnt->save();                
            }
        }
        return View::make('upload', [ 'erros' => $erros ]);
    }   

}