<?php

class UsuarioController extends BaseController {

    public function __construct() {        
        $this->beforeFilter('csrf', array('on' => 'post'));
    }

    public function postLogin()
    {
        if (Auth::attempt(array('user_name' => input::get('user_name'), 'password' => input::get('password')))) 
        {
            return Redirect::intended('/');
        }
    }   

    public function getLogout() 
    {
        Auth::logout();
        return Redirect::to('/');
    }

}