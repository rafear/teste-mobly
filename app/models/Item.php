<?php

class Item extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'item';	

	/**
     * Verifica os campos que desejam que sejam inseridos
     * @param array valore que serão inseridos
     * @return Validator classe de validacão
     */
    public static function validate($arrValues, $purchase_order_id) {
        
        Validator::extend('custom_unique', function($attribute, $value, $parameters)
        {
            $total = Item::where('item_number', '=', $value)
                ->where('purchase_order_id', '=', $parameters[0])->count();
            if($total > 0) {
                return false;
            }  else {
                return true;
            }
        });

        return $validator = Validator::make(
            $arrValues,
            array(
                'item_number' => 'required|integer|max:999|custom_unique:'.$purchase_order_id,
                'cost' => 'required|numeric',
                'discount' => 'required|numeric',
                'purchase_order_id' => 'required',
                'created_at' => 'date',
                'updated_at' => 'date'

            )
        );
    }


	public function compra()
    {
        return $this->belongsTo('Purchase_order', 'purchase_order_id', 'id');
    }
}