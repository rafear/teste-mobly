<?php

class Purchase_order extends Eloquent {

	/**
	 * The database table used by the model.
	 *
	 * @var string
	 */
	protected $table = 'purchase_order';	

	public function items() {
        return $this->hasMany('Item');
    }

    /**
     * Verifica os campos que desejam que sejam inseridos
     * @param array valore que serão inseridos
     * @return Validator classe de validacão
     */
    public static function validate($arrValues) {
        return $validator = Validator::make(
            $arrValues,
            array(
                'order_number' => 'required|integer|max:9999999999|unique:purchase_order',
                'total_cost' => 'numeric',
                'total_discount' => 'numeric',
                'created_at' => 'date',
                'updated_at' => 'date',
            )
        );
    }

    public static function atualiza_valores($purchase_order_id) {
    	$listaItens = Item::where('purchase_order_id', '=', $purchase_order_id)->get();
    	$sumTotal = 0;
    	$sumDesconto = 0;
    	foreach ($listaItens as $item) {
    		$sumTotal += $item->cost;
    		$sumDesconto += $item->discount;    	
    	}

    	$compra = Purchase_order::find($purchase_order_id);
		$compra->total_cost = $sumTotal;
		$compra->total_discount = $sumDesconto;
		$compra->save();
    }
}