@extends('layouts.master')

@section('content')
    <div class="starter-template">
        <h1>Login</h1>
        <p class="lead">Você foi redirecionado para essa página, por favor efetue o login</p>
        {{ Form::open(array('url' => 'usuario/login', 'role' => 'form')) }}
        	<div class="form-group">
              <input type="text" placeholder="Usuário" name="user_name" class="form-control" />
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" name="password" class="form-control" />
            </div>
            <button type="submit" class="btn btn-success">Login</button>
		{{ Form::close() }}
        
      </div>
@stop
	 
