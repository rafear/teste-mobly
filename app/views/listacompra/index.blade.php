@extends('layouts.master')

@section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>listas</h1>
        <p class="lead">Segue lista de Compras realizadas.</p>
        {{Form::button('<i class="glyphicon glyphicon-shopping-cart"></i> Incluir Compra', array('class' => 'btn incluir'))}}
        <table class="table table-striped">
        	<thead>
        		<tr>
        			<th>Código</th>
        			<th>Número de Itens</th>
        			<th>Desconto</th>
        			<th>Total</th>
        			<th>Ações</th>
        		</tr>
        	</thead>
        	<tbody>
                @foreach($listas as $lista)
        		<tr>
        			<td>{{$lista['order_number']}}</td>
                    <td>{{$lista->items()->count()}}</td>
                    <td>{{$lista['total_discount']}}</td>
                    <td>{{$lista['total_cost']}}</td>
                    <td>
                        {{Form::button('<i class="glyphicon glyphicon-pencil"></i>', array('class' => 'btn editar', 'value' => $lista['id']))}}
                        {{Form::button('<i class="glyphicon glyphicon-list-alt"></i>', array('class' => 'btn detalhes', 'value' => $lista['id']))}}
                        {{Form::button('<i class="glyphicon glyphicon-remove"></i>', array('class' => 'btn excluir', 'value' => $lista['id']))}}
                    </td>
        		</tr>
                @endforeach
        	</tbody>
        </table>
      </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $('.incluir').click(function(){
                window.location="{{action('ListacompraController@getCreate');}}";
            });


            $('.editar').click(function(){
                window.location="{{action('ListacompraController@getEdit');}}/"+$(this).attr('value');
            });


            $('.detalhes').click(function(){
                window.location="{{action('ItemController@getIndex');}}/"+$(this).attr('value');
            });


            $('.excluir').click(function(){
                window.location="{{action('ListacompraController@getDestroy');}}/"+$(this).attr('value');
            });
            
        });
    </script>

@stop
