@extends('layouts.master')

@section('content')
	<div class="row">
	  <div class="col-md-8 col-md-offset-2">
		<h1>Criação de Lista</h1>
        

        		
        @if($errors->count() > 0)
        <h3> Ocorreram os seguintes erros:</h3>
        <ul>
        @foreach($errors->getMessageBag()->toArray() as $campo => $messagens)        	
			<li> No campo <b>{{$campo}}</b>:
				<ol>				
        			@foreach($messagens as $mensagem) 
        				<li>{{$mensagem}}</li>
        			@endforeach
				</ol>
			</li>
        @endforeach
        </ul>
        @endif
        <p class="lead">Crie um código unico para a sua lista de Compras.</p>
        {{ Form::open(array('url' => 'listacompra/store', 'role' => 'form')) }}
        	<div class="form-group">
              <input type="text" placeholder="1234" name="order_number" class="form-control" />
            </div>
        	{{Form::button('<i class="glyphicon glyphicon-cancel"></i> Cancelar', array('class' => 'btn btn_sucess cancelar'))}}
        	&nbsp; 
        	{{Form::submit('Incluir Compra', array('class' => 'btn btn_sucess'))}}
		{{ Form::close() }}
	  </div>
	</div>
    <script type="text/javascript">
        $(function() {
            $('.cancelar').click(function(){
                window.location="{{action('ListacompraController@getIndex')}}";
            });           
            
        });
    </script>
@stop
