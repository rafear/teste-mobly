@extends('layouts.master')

@section('content')
	<div class="row">
	  <div class="col-md-8 col-md-offset-2">
		<h1>Atualizacão da Compra{{$listaCompra->order_number}}</h1>
        		
        @if($errors->count() > 0)
        <h3> Ocorreram os seguintes erros:</h3>
        <ul>
        @foreach($errors->getMessageBag()->toArray() as $campo => $messagens)        	
			<li> No campo <b>{{$campo}}</b>:
				<ol>				
        			@foreach($messagens as $mensagem) 
        				<li>{{$mensagem}}</li>
        			@endforeach
				</ol>
			</li>
        @endforeach
        </ul>
        @endif
        <p>&nbsp;</p>
        {{ Form::model($listaCompra, array('action' => array('ListacompraController@postUpdate', $listaCompra->id)))}}
            <div class="form-group">
                {{Form::label('order_number', 'Ordem')}}                
                {{Form::text('order_number', $listaCompra->order_number, array('class' => 'form-control', 'placeholder' => '123'))}}
            </div>
            {{Form::button('<i class="glyphicon glyphicon-cancel"></i> Cancelar', array('class' => 'btn btn_sucess cancelar'))}}
            &nbsp; 
        	{{Form::submit('Atualizar', array('class' => 'btn btn_sucess'))}}
		{{ Form::close() }}
	  </div>
	</div>
    <script type="text/javascript">
        $(function() {
            $('.cancelar').click(function(){
                window.location="{{action('ListacompraController@getIndex')}}";
            });           
            
        });
    </script>
@stop
