@extends('layouts.master')

@section('content')
	<div class="row">
	  <div class="col-md-8 col-md-offset-2">
		<h1>Criação de Item</h1>
        

        		
        @if($errors->count() > 0)
        <h3> Ocorreram os seguintes erros:</h3>
        <ul>
        @foreach($errors->getMessageBag()->toArray() as $campo => $messagens)        	
			<li> No campo <b>{{$campo}}</b>:
				<ol>				
        			@foreach($messagens as $mensagem) 
        				<li>{{$mensagem}}</li>
        			@endforeach
				</ol>
			</li>
        @endforeach
        </ul>
        @endif
        <p>Inclusão de itens, diretamente em uma compra.</p>
        {{ Form::open(array('url' => 'item/store', 'role' => 'form')) }}
        	{{Form::hidden('compra_id', $compra_id)}}
            <div class="form-group">
                {{Form::label('purchase_order_id', 'Compra');}}
                {{Form::select('purchase_order_id', $lista_compra, $compra_id, array('class' => 'form-control', 'readonly'))}}
            </div>
            <div class="form-group">
                {{Form::label('item_number', 'Ordem')}}                
                {{Form::text('item_number', '', array('class' => 'form-control', 'placeholder' => '123'))}}
            </div>
            <div class="form-group">
                {{Form::label('cost', 'Preço')}}
                {{Form::text('cost', '', array('class' => 'form-control', 'placeholder' => '00.00'))}}
            </div>
            <div class="form-group">
                {{Form::label('discount', 'Desconto')}}
                {{Form::text('discount', '', array('class' => 'form-control', 'placeholder' => '00.00'))}}
            </div>
        	{{Form::button('<i class="glyphicon glyphicon-cancel"></i> Cancelar', array('class' => 'btn btn_sucess cancelar'))}}
        	&nbsp; 
        	{{Form::submit('Incluir Item', array('class' => 'btn btn_sucess'))}}
		{{ Form::close() }}
	  </div>
	</div>
    <script type="text/javascript">
        $(function() {
            $('.cancelar').click(function(){
                window.location="{{action('ItemController@getIndex', [$compra_id])}}";
            });           
            
        });
    </script>
@stop
