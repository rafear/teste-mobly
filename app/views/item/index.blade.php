@extends('layouts.master')

@section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Itens</h1>
        @if($purchase_order_id)   
            <p>Segue lista dos itens de compra realizadas da compra {{$purchase_order_id}}.</p>
        @else
            <p>Segue lista dos itens de compra realizadas.</p>
        @endif
        {{Form::button('<i class="glyphicon glyphicon-paperclip"></i> Incluir Item', array('class' => 'btn incluir', 'value' => $purchase_order_id))}}
        @if($purchase_order_id)   
            {{Form::button('Concluir', array('class' => 'btn concluir'))}}        
        @endif
        <table class="table table-striped">
        	<thead>
        		<tr>
        			@if(!$purchase_order_id)                    
                    <th>Compra</th>
                    @endif
                    <th>Ordem</th>
        			<th>Desconto</th>
                    <th>Valor</th>
        			<th>Ações</th>
        		</tr>
        	</thead>
        	<tbody>
                @foreach($itens as $item)
        		<tr>

                    @if(!$purchase_order_id)   
                    <td>{{$item->compra->order_number}}</td>
                    @endif
                    <td>{{$item['item_number']}}</td>
                    <td>{{$item['discount']}}</td>
                    <td>{{$item['cost']}}</td>
                    <td>
                        {{Form::button('<i class="glyphicon glyphicon-pencil"></i>', array('class' => 'btn editar', 'value' => $item['id']))}}
                        {{Form::button('<i class="glyphicon glyphicon-remove"></i>', array('class' => 'btn excluir', 'value' => $item['id']))}}
                    </td>
        		</tr>
                @endforeach
        	</tbody>
        </table>
      </div>
    </div>
    <script type="text/javascript">
        $(function() {
            $('.incluir').click(function(){
                window.location="{{action('ItemController@getCreate')}}/"+$(this).attr('value');
            });

            $('.concluir').click(function(){
                window.location="{{action('ListacompraController@getIndex')}}";
            });

            $('.editar').click(function(){
                window.location="{{action('ItemController@getEdit')}}/"+$(this).attr('value')+"/{{$purchase_order_id}}";
            });


            $('.excluir').click(function(){
                window.location="{{action('ItemController@getDestroy')}}/"+$(this).attr('value')+"/{{$purchase_order_id}}";
            });
            
        });
    </script>

@stop
