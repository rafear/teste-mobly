@extends('layouts.master')

@section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Teste de Programação para a Empresa Mobly</h1>
        
        <p>
        	Seguindo as instruções solicitadas, segue aqui a demonstração de código gerada.        	
        </p>
        <ul>
        	<li>Criação das Entidades Purchase_order e Item, além de seus respectivos CRUDs;</li>
        	<li>Utilizar um framework dos listados</li>
        	<li>Utilizar o Twitter Bootstrap</li>
        	<li>Utilizar Banco de Dados MySql</li>
        	<li>Persistir Dados de um Xml</li> 		
        </ul>

      </div>
    </div>
@stop
	 
