@extends('layouts.master')

@section('content')
    <div class="row">
      <div class="col-md-8 col-md-offset-2">
        <h1>Upload de Arquivo Xml</h1>
        <br />
        <br />
        <p>O script foi executado com sucesso.</p>
        @if(!empty($erros))        
        <p><b>Ocorreram os seguintes erros:</b></p>
            @foreach($erros as $erro)
        <p>{{$erro['texto']}}</p>
        <pre>{{$erro['classe']}}</pre>    
            @endforeach
        @endif
      </div>
    </div>
@stop
	 
