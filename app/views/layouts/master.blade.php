<!DOCTYPE html>
<html lang="pt_br">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Teste Mobly de Programação</title>
    <link rel="icon" type="image/ico" href="favicon.ico" />
	  <link rel="shortcut icon" href="favicon.ico" />
    {{HTML::style('css/bootstrap.min.css', array('rel' => 'stylesheet'))}}
    {{HTML::style('css/main.css', array('rel' => 'stylesheet'))}}

    <!-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      {{HTML::style('https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js');}}
      {{HTML::style('https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js');}}
    <![endif]-->    
    {{HTML::script('https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js');}}
    {{HTML::script('js/bootstrap.min.js');}}
  </head>
  <body>
  	<div class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-collapse">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Teste Mobly</a>
        </div>
        <div class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li class="{{{ Request::path() === '/' ? 'active' : '' }}}">{{link_to('/', 'Home');}}</li>
            <li class="{{{ strstr(Request::path(), 'listacompra') !== false ? 'active' : '' }}}">{{link_to('listacompra', 'Compra');}}</li>
            <li class="{{{ strstr(Request::path(), 'item') !== false ? 'active' : '' }}}">{{link_to('item', 'Itens');}}</li>
            <li class="{{{ strstr(Request::path(), 'uploadxml') !== false ? 'active' : '' }}}">{{link_to('uploadxml', 'Upload Xml');}}</li>
          </ul>
          @if(Auth::check())
          <ul class="nav navbar-nav pull-right">
            <li>{{link_to('usuario/logout', 'Sair');}}</li>
          </ul>
          @endif
        </div><!--/.nav-collapse -->
      </div>
    </div>

    <div class="container">
      @yield('content')     
    </div>
  </body>
</html>